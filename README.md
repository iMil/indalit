## Indalit website

Here you'll find the source code of my company's website, [Indalit][1].

This website uses:

* [Bootstrap][2]
* [VueJS][3]

[1]: https://www.indalit.com
[2]: https://getbootstrap.com/
[3]: https://vuejs.org/
