indalitdesc = `
  _Indalit_ is a consulting company with **more than 20 years** of experience in the IT field and more specifically Internet based services.  
  We are specialized in **UNIX & Linux** based operating systems, with a great deal of understanding of **complex network architectures** and deep knowledge of **Cloud Computing**.
`

punchline = `
  25 years of experience, consulting for the following technologies
`

cryptotooltip = `
  **Blockchain**

  "Blockchain" is not a buzzword to us, instead it is the very present revolution occuring to finance, supply chain and decentralization. We've been involved in the blockchain technology since 2017 and understand it to its core level, along with its advanced applications using Smart Contracts.

  _Tags:_ \`[Blockchain, Smart Contracts, Bitcoin, Ethereum]\`
`

awstooltip = `
  **Amazon Web Services**

  We use AWS for about a decade, meaning that we deployed hundreds of platforms with many different AWS products and various deployment tools such as _Ansible_, _Salt_, _Terraform_ or _aws_'s _boto3_.

  _Tags:_ \`[EC2, AutoScaling, S3, Lambda, CloudFormation]\`
`

codetooltip = `
  **Development**

  Code is life. Our releases and contributions speak for themselves, from C to Go along with python, we've been contrinuting on many OpenSource projects, and quality have always been our #1 target.

  _Tags:_ \`[C, Go, Python, Shell]\`
`

linuxtooltip = `
  **GNU/Linux**

  The most renown Open Source Operating System around, runs on the supercomputers top 500 alongside your Android mobile phone. We've been using GNU/Linux since 1996 both as a personnal operating system and on production servers, we **know** Linux.

  _Tags:_ \`[Debian, Ubuntu, RedHat, CoreOS]\`
`

bsdtooltip = `
  **BSD UNIX**

  NetBSD, FreeBSD and OpenBSD: where Open Source UNIX-like systems began to be a thing. Our team contributed extensively to various aspects of those legendary Operating Systems, including NetBSD's binary package manager.

  _Tags:_ \`[NetBSD, FreeBSD, OpenBSD, pkgsrc]\`
`

dockertooltip = `
  **Docker**

  Docker is not meant to be a trashcan where you drop any application you're not able to package, its real beauty precisely lies in the ability to strip down and secure any service, plus making it loadable pretty much anywhere.

  _Tags:_ \`[Container, docker, docker-compose, Kubernetes]\`
`

kubernetestooltip = `
  **Kubernetes**

  _Kubernetes_ has landed as nothing less than the de facto container orchestrator. Since Google Open Sourced it, its popularity and adoption are exponetial, we've setup and audited various k8s platforms since 2017, and we have made a couple of online conference on the matter.

 _Tags:_ \`[Kubernetes, orchestration, kubectl, EKS]\`
`

var indalit = new Vue({
  el: '#indalit',
  data: {
    technos: [
      {
        link: 'https://gitlab.com/iMil',
        img: 'images/c_logo.png',
        alt: 'Development',
        tooltip: codetooltip
      },
      {
        link: 'https://www.unitedbsd.com/',
        img: 'images/beastie_logo.png',
        alt: 'BSD UNIX',
        tooltip: bsdtooltip
      },
      {
        link: 'https://linux.com/',
        img: 'images/linux_logo.png',
        alt: 'GNU/Linux',
        tooltip: linuxtooltip
      },
      {
        link: 'https://github.com/iMilnb/awstools',
        img: 'images/aws_logo.png',
        alt: 'AWS',
        tooltip: awstooltip
      },
      {
        link: 'https://www.youtube.com/watch?v=fbuk8q-6XYE',
        img: 'images/k8s_logo.png',
        alt: 'Kubernetes',
        tooltip: kubernetestooltip
      },
      {
        link: 'https://imil.net/goxplorer/',
        img: 'images/blockchain_logo.png',
        alt: 'Blockchain',
        tooltip: cryptotooltip
      }
    ],
    indalittooltip: indalitdesc
  },

  mounted: function() {
    $('[data-toggle="tooltip"]').tooltip();
  }
})
